package integration

import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.util.Timeout
import services.XucStatsEventBus
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import xivo.models.XivoObject.ObjectDefinition
import models.QueueLog.{ EnterQueue, Abandonned }
import stats.Statistic
import xivo.data.QLogTransfer
import models.QueueLog
import xivo.data.QLogDispatcher
import models.QLogTransformer
import xivo.data.EvtDispatcher
import xivo.data.ProductionEvtDispatcher

object UseStatistics extends App {
  import xivo.models.XivoObject.ObjectType._

  class StatReceiver extends Actor {
    def receive = {
      case msg => println(s"stat received $msg")
    }
  }

  val appliConfig: Map[String, Any] = {
    Map(
      "logger.root" -> "DEBUG",
      "logger.application" -> "DEBUG",
      "logger.services" -> "DEBUG",
      "akka.loggers" -> List("akka.event.slf4j.Slf4jLogger"),
      "log-dead-letters-during-shutdown" -> "off",
      "loglevel" -> "DEBUG",
      "db.default.driver" -> "org.postgresql.Driver",
      "db.default.url" -> "jdbc:postgresql://192.168.56.3/asterisk",
      "db.default.user" -> "asterisk",
      "db.default.password" -> "proformatique")
  }

  running(FakeApplication(additionalConfiguration = appliConfig)) {
    val qlogDispatcher = Akka.system.actorOf(Props(new QLogDispatcher(QLogTransformer.transform) with ProductionEvtDispatcher), "QLogDispatcher")
    val qlogTransfer = Akka.system.actorOf(Props(new QLogTransfer(QueueLog.getAll, qlogDispatcher)), "QLogTransfer")

    Thread.sleep(2000)

    val rcv1 = Akka.system.actorOf(Props(new StatReceiver))

    XucStatsEventBus.statEventBus.subscribe(rcv1, ObjectDefinition(Queue, None))

    Thread.sleep(500000)
    Akka.system.shutdown
  }
}