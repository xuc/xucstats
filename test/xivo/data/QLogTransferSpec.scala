package xivo.data

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.QueueLog.QueueLogData
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import stats.Statistic.ResetStat

class QLogTransferSpec extends TestKitSpec("QLogTransferSpec")
  with MockitoSugar {

  class Helper {
    val getQueueLog = mock[(String) => List[QueueLogData]]
    val qLogDispatcher = TestProbe()

    def actor() = {
      val a = TestActorRef(new QLogTransfer(getQueueLog, qLogDispatcher.ref))
      (a, a.underlyingActor)
    }
    val appliConfig: Map[String, Any] = {
      Map(
        "logger.root" -> "OFF",
        "logger.application" -> "OFF",
        "logger.services" -> "OFF",
        "akka.loggers" -> List("akka.event.slf4j.Slf4jLogger"),
        "log-dead-letters-during-shutdown" -> "off",
        "loglevel" -> "OFF")
    }
  }

  "Qlog transfer" should {
    import xivo.data.QLogTransfer.LoadQueueLogs
    "get all queue log with default query for the first time" in new Helper {
      running(FakeApplication(additionalConfiguration = appliConfig)) {

        import models.QueueLog.defaultClause

        val (ref, qLogTransfer) = actor()
        when(getQueueLog(anyString)).thenReturn(List())

        ref ! LoadQueueLogs()

        verify(getQueueLog)(defaultClause)
      }
    }

    "setup next clause with the last time stamp received" in new Helper {
      val (ref, qLogTransfer) = actor()

      val lastQueueLogReceived = QueueLogData("2014-04-18 12:22:20.225112", "Agent/1702", "WRAPUPSTART")
      when(getQueueLog(anyString)).thenReturn(List(lastQueueLogReceived))

      ref ! LoadQueueLogs()

      qLogTransfer.nextClause should be(s"'${lastQueueLogReceived.queuetime}'")
    }
    "send reset stat on startup" in new Helper {
      val (ref, _) = actor()

      qLogDispatcher.expectMsg(ResetStat)
    }
    "send each queue log event received to queuelog dispatcher" in new Helper {
      val (ref, _) = actor()

      val queueLogReceived = QueueLogData("2014-04-18 12:22:20.225112", "Agent/1702", "WRAPUPSTART")
      when(getQueueLog(anyString)).thenReturn(List(queueLogReceived))

      ref ! LoadQueueLogs()

      qLogDispatcher.expectMsgAllOf(ResetStat, queueLogReceived)
    }
  }
}