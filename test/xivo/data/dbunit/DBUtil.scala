package xivo.data.dbunit

import org.dbunit.database.{ DatabaseConfig, DatabaseConnection }
import org.dbunit.operation.DatabaseOperation
import org.dbunit.dataset.xml.FlatXmlDataSet
import anorm._
import play.api.db.DB
import play.api.Play.current
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
import org.dbunit.dataset.xml.FlatXmlProducer
import org.xml.sax.InputSource

object DBUtil {
  private var dbunitConnection: DatabaseConnection = null

  def setupDB(fileName: String) = {
    DB.withConnection { implicit conn =>

      try {
        this.dbunitConnection = new DatabaseConnection(conn)
        this.dbunitConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory)

        val dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(inputFromFile(fileName))))

        for (table <- dataset.getTableNames()) {
          val createTable = scala.io.Source.fromInputStream(inputFromFile(s"${table}.sql")).mkString
          SQL(createTable).execute
        }
        DatabaseOperation.CLEAN_INSERT.execute(this.dbunitConnection, dataset)
      } catch {
        case e: Exception => println("-------------------" + e.getMessage())
      }
    }
  }
  private def inputFromFile(fileName:String) = getClass().getClassLoader().getResourceAsStream(fileName)
}