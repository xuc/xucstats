package stats

import akka.actor.{Actor, ActorLogging, actorRef2Scala}
import models.QueueLog._
import stats.Statistic.{ResetStat, RequestStat}


trait StatCreator {
  val queueStatRepository:QueueStatRepository
}

trait ProdStatCreator extends StatCreator {
  val queueStatRepository = new QueueStatRepository
}

class QueueEventDispatcher extends Actor with ActorLogging {
  this:StatCreator =>

  def receive = {
    case EnterQueue(queueName) => queueStatRepository.getQueueStat(queueName) match {
      case Some(actorRef) => actorRef ! EnterQueue(queueName)
      case None =>
        log.info(s"unknown queueName $queueName: creating statistic")
        queueStatRepository.createQueueStat(queueName, context) ! EnterQueue(queueName)
    }

    case Abandonned(queueName,waitingTime) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ !Abandonned(queueName,waitingTime))

    case Complete(queueName,agentNumber,waitTime,callTime) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ ! Complete(queueName,agentNumber,waitTime,callTime))

    case Connect(queueName,agentNumber,waitTime,callId) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ ! Connect(queueName,agentNumber,waitTime,callId))

    case Closed(queueName) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ ! Closed(queueName))

    case Timeout(queueName, waitingTime) =>
      queueStatRepository.getQueueStat(queueName).foreach(_ ! Timeout(queueName, waitingTime))

    case requestStat:RequestStat =>
      log.debug(s"Statistics request $requestStat ${queueStatRepository.getAllStats.size}")
      queueStatRepository.getAllStats.foreach(_ ! requestStat)

    case ResetStat =>
      log.info(s"Statistics reset requested ${queueStatRepository.getAllStats.size}")
      queueStatRepository.getAllStats.foreach(_ ! ResetStat)
  }
}