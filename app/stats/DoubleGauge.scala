package stats

import com.codahale.metrics.Gauge

class DoubleGauge extends Gauge[Double] {
  private var value: Double = 0.0

  def setValue(data: Double) = {
    value = data
  }

  def getValue: Double = {
    value
  }
}
