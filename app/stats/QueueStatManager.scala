package stats

import akka.actor.{ Actor, ActorLogging, Props }
import stats.Statistic.PublishSlidingStats
import xivo.models.XivoObject.ObjectDefinition
import services.{ CronScheduler, ProductionCronScheduler }
import us.theatr.akka.quartz.AddCronSchedule
import xivo.xucstats.XucStatsConfig
import akka.actor.ActorRef

object QueueStatManager {
  def apply(queueId: Int) = new QueueStatManager(queueId) with ProductionCronScheduler
}

class QueueStatManager(queueId: Int) extends Actor with ActorLogging {
  this: CronScheduler =>
  import xivo.models.XivoObject.ObjectType._
  import Statistic.ResetStat

  val WaitTimeThreshold = 15
  val PublishSlidingSchedule = "0 * * * * ?"


  var stats: List[ActorRef] = List()

  override def preStart() {
    log.info(s"setting schedule to ${XucStatsConfig.resetSchedule}")
    scheduler ! AddCronSchedule(self, XucStatsConfig.resetSchedule, ResetStat)
    scheduler ! AddCronSchedule(self, PublishSlidingSchedule, PublishSlidingStats)
  }

  val aggregator = context.actorOf(StatsAggregator.props(), s"StatsAggregator-$queueId")

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsEntered", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsEntered, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsAbandonned", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAbandonned, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"TotalNumberCallsAbandonnedAfter$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAbandonnedAfter(WaitTimeThreshold), aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsAnswered", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAnswered, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"TotalNumberCallsAnsweredBefore$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAnsweredBefore(WaitTimeThreshold), aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAnsweredBefore$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAnsweredBefore(WaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAbandonnedAfter$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAbandonnedAfter(WaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsClosed", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsClosed, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsTimeout", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsTimeout, aggregator) with HistoSize)) :: stats

  def receive = {
    case event => stats.foreach(_ ! event)
  }
}