package stats

import akka.actor.{Actor, ActorLogging, Props}
import services.XucStatsEventBus
import services.XucStatsEventBus.{AggregatedStatEvent, StatUpdate}
import xivo.models.XivoObject.ObjectDefinition
import xivo.xucstats.XucStatsConfig

import scala.concurrent.duration._

object StatsAggregator {
  def props(): Props = Props(new StatsAggregator(XucStatsEventBus.statEventBus))

  case object Flush
  case object SubscribeToBus
}

class StatsAggregator(statsBus: XucStatsEventBus) extends Actor with ActorLogging{

  import context._
  import stats.StatsAggregator._

  log.debug(s"statagregator $self starting")

  def receive = NoBuffer


  def NoBuffer : Receive = {
    case SubscribeToBus =>
      log.debug("Subscribing to the bus")
      import xivo.models.XivoObject.ObjectType._
      statsBus.subscribe(self, ObjectDefinition(Queue))

    case statUpdate: StatUpdate =>
      log.debug(s"Publishing stat $statUpdate")
      send(AggregatedStatEvent(statUpdate.xivoObject, statUpdate.stat))
      armFlushTimer
      become(buffering(new StatisticBuffer))
  }


  def buffering(buffer: StatisticBuffer): Receive = {
    case statUpdate: StatUpdate =>
      log.debug(s"Buffering stat: $statUpdate")
      become(buffering(buffer.addStat(statUpdate)))

    case Flush =>
      if (buffer.isEmpty ) {
        log.debug("Flush : StatBuffer empty")
        become(receive)
      } else {
        log.debug(s"Flush : Publishing statBuffer $buffer")
        sendBuffer(buffer)
        armFlushTimer
        become(buffering(new StatisticBuffer))
      }
  }

  private def sendBuffer(buffer: StatisticBuffer) {
    buffer.aggregate.foreach(send(_))
  }

  private def armFlushTimer = {
    context.system.scheduler.scheduleOnce(XucStatsConfig.aggregationPeriod second) {
      self ! Flush
    }
  }
  private def send(aggregatedStat: AggregatedStatEvent) {
    statsBus.publish(aggregatedStat)
  }
}

