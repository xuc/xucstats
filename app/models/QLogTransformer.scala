package models

import models.QueueLog._

object QLogTransformer {
  type QLogDataTransformer = (QueueLogData) => ObjectEvent

  def parseInt(s:String): Int = {
    try {
      s.toInt
    } catch {
      case e:NumberFormatException => 0
    }
  }

  def transform(qLogData: QueueLogData): ObjectEvent = {
    qLogData match {
      case QueueLogData(_, _, "ENTERQUEUE", _, _, _, Some(queueId)) => EnterQueue(queueId)

      case QueueLogData(_, _, "ABANDON", _, _, Some(waitingTime), Some(queueId)) => Abandonned(queueId, waitingTime.toInt)

      case QueueLogData(_, _, "EXITWITHTIMEOUT", _, _, Some(waitingTime), Some(queueId)) => Timeout(queueId, parseInt(waitingTime))

      case QueueLogData(_, agentRef, "COMPLETEAGENT", Some(waitingTime), Some(callDuration), _, Some(queueId)) =>
        Complete(queueId, toAgentNumber(agentRef), waitingTime.toInt, callDuration.toInt)

      case QueueLogData(_, agentRef, "COMPLETECALLER", Some(waitingTime), Some(callDuration), _, Some(queueId)) =>
        Complete(queueId, toAgentNumber(agentRef), waitingTime.toInt, callDuration.toInt)

      case QueueLogData(_, agentRef, "CONNECT", Some(waitingTime), Some(callId), _, Some(queueId)) =>
        Connect(queueId, toAgentNumber(agentRef), waitingTime.toInt, callId)

      case QueueLogData(_, _, "CLOSED", _, _, _, Some(queueId)) =>
        Closed(queueId)

      case _ => Unknown
    }
  }

  private def toAgentNumber(agent: String) = agent.split("/").last
}